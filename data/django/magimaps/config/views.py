from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from django.contrib.auth.forms import UserCreationForm
from allauth.account.views import *
from django.contrib.auth.mixins import LoginRequiredMixin

##def map(request):
#    if request.user.is_authenticated:
#        template_name = 'pages/map.html'
#        accounts = AccountSettingsView
#        return render(request, template_name, accounts)
#        #return render(request, 'pages/map.html')
#    else:
#        return redirect('/')

    
class JointLoginSignupView(LoginView):
    form_class = LoginForm
    signup_form = SignupForm
    change_password = ChangePasswordForm
    def get_context_data(self, **kwargs):
        ret = super(JointLoginSignupView, self).get_context_data(**kwargs)
        ret['signupform'] = get_form_class(app_settings.FORMS, 'signup', self.signup_form)
        ret['changepasswordform'] = get_form_class(app_settings.FORMS, 'changepassword', self.change_password)
        return ret
 
login = JointLoginSignupView.as_view()

class AccountSettingsView(LoginRequiredMixin, PasswordChangeView):
    form_class = ChangePasswordForm
    email_form = AddEmailForm

    def get_context_data(self, **kwargs):
        ret = super(AccountSettingsView, self).get_context_data(**kwargs)
        ret['addemailform'] = get_form_class(app_settings.FORMS, 'add_email', self.email_form)
        return ret
    
