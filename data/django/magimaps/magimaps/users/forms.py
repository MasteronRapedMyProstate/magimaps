from django import forms

class SignupForm(forms.Form):
    email = forms.CharField(max_length=254)
    password1 = forms.CharField(widget=forms.PasswordInput)
    
    def __init__(self, *args, **kwargs):
        super(SignupForm, self).__init__( *args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False
        self.helper.field_class = ''
        self.helper.layout = Layout(
            Field('email', placeholder='Email', autocomplete='off'),
            Field('password1', placeholder='Password', autocomplete='off'),
            
            Div(Submit('Register', 'Register', css_class='btn btn-primary block full-width m-b'), css_class='form-group'),
            HTML('<p class="text-muted text-center"><small>Already have an account?</small></p>'),
            Div(HTML('<a class ="btn btn-sm btn-white btn-block" href="' + reverse('account_login') + ' " > Login </a>'),css_class='form-group' )
        )

    def signup(self, request, user):
        user.email = self.cleaned_data['email']
        user.save()
