{% load staticfiles %}
/* Burger JS functionality */
  document.getElementById('burger-button').addEventListener("click", function () {
  document.getElementById('burger-menu').classList.toggle('open');
  
  var srcRegex = /\/burger\.svg/;
  var logoRegex = /\/logo\.svg/;
  
  if (srcRegex.test(document.getElementById('burger-button-img').src)) {
  document.getElementById('burger-button-img').src = "{% static 'images/x.svg' %}"}
  else {
  document.getElementById('burger-button-img').src = "{% static 'images/burger.svg' %}"}
  
  if (logoRegex.test(document.getElementById('logo').src)) {
  document.getElementById('logo').src = "{% static 'images/favicons/favicon.svg' %}";
  document.getElementById('logo').height = "40";
  document.getElementById('logo').width = "48";}
  else {
  document.getElementById('logo').src = "{% static 'images/logo.svg' %}";
  document.getElementById('logo').height = "77";
  document.getElementById('logo').width = "76";}
  });

