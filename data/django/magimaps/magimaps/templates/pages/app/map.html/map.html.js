/* Main "/map" JS Scipt */

// Set each land mass' coordinates manually for reuse
var locations = {
    "continental":[[-10711508.68211889, 4719970.639550782],4.8],
    "hawaii":[[-17540685.94711121, 2356805.704029599],7],
    "alaska":[[-17674724.155051, 9200842.735874468],4.7],
    "puertoRico":[[-7414647.280814212, 2070310.817759723],9],
    "continental&hawaii":[[-12797399.819382252, 4632130.2356878305],4.1],
    "continental&puertoRico":[[-10576270.007714132, 4667962.985318984],4.8],
    "continental&alaska":[[-14164996.803412782, 7129800.121393248],3.8],
    "alaska&hawaii":[[-17187432.650943533, 6884656.234266592],4],
    "alaska&puertoRico":[[-13989207.912757862, 7104868.158723744],3.7],
    "hawaii&puertoRico":[[-12385587.329747995, 2342177.677869584],4],
    "all":[[-13876728.507846633, 6897031.050562585],3.7]
}

// Continental layer inits
var continental = new ol.source.TileWMS({
    url: 'http://127.0.0.1/wms/',
    params: {'LAYERS': 'continental', 'TILED': true, 'MAP': '/var/www/maps/2.map'},
    projection: 'EPSG:3857',
    serverType: 'mapserver',
    crossOrigin: 'anonymous',
    wrapX: false,
});

var continentalLayer = new ol.layer.Tile({
    source: continental,
    visible: false,
    zIndex: 1
});

// Alaska layer inits
var alaska = new ol.source.TileWMS({
    url: 'http://127.0.0.1/wms/',
    params: {'LAYERS': 'alaska', 'TILED': true, 'MAP': '/var/www/maps/2.map'},
    projection: 'EPSG:3857',
    serverType: 'mapserver',
    crossOrigin: 'anonymous',
    wrapX: true,
});

var alaskaLayer = new ol.layer.Tile({
    source: alaska,
    visible: false,
    zIndex: 1
});

// Puerto Rico layer inits
var puertoRico = new ol.source.TileWMS({
    url: 'http://127.0.0.1/wms/',
    params: {'LAYERS': 'puertoRico', 'TILED': true, 'MAP': '/var/www/maps/2.map'},
    projection: 'EPSG:3857',
    serverType: 'mapserver',
    crossOrigin: 'anonymous',
    wrapX: false,
});

var puertoRicoLayer = new ol.layer.Tile({
    source: puertoRico,
    visible: false,
    zIndex: 1
});

// Hawaii layers inits
var hawaii = new ol.source.TileWMS({
    url: 'http://127.0.0.1/wms/',
    params: {'LAYERS': 'hawaii', 'TILED': true, 'MAP': '/var/www/maps/2.map'},
    projection: 'EPSG:3857',
    serverType: 'mapserver',
    crossOrigin: 'anonymous',
    wrapX: false,
});

var hawaiiLayer = new ol.layer.Tile({
    source: hawaii,
    visible: false,
    zIndex: 1
});

// Results layer overlays above everything else
// Results layer inits

// WMS Version
/*var psql = new ol.source.TileWMS({
    url: 'http://127.0.0.1/wms/',
    params: {'LAYERS': 'psql', 'TILED': true, 'MAP': '/var/www/maps/2.map'},
    projection: 'EPSG:3857',
    serverType: 'mapserver',
    crossOrigin: 'anonymous',
    wrapX: false,
});

var psqlLayer = new ol.layer.Tile({
    source: psql,
    visible: false
});*/


/*var psql = new ol.source.Vector({
    url: 'http://127.0.0.1/wms/',
    params: {'LAYERS': 'psql', 'TILED': false, 'MAP': '/var/www/maps/wfs.map'},
    projection: 'EPSG:3857',
    format: new ol.format.GeoJSON(),
    serverType: 'mapserver',
    crossOrigin: 'anonymous',
    wrapX: false,
});

var psqlLayer = new ol.layer.Vector({
    source: psql,
    visible: false
});*/

// View layer with coords centered on the USA
var view = new ol.View({
    projection: 'EPSG:3857',
    center: [-10997148, 4569099],
    zoom: 5,
    minZoom: 3.5,
});

// Start off the map with all layers on
var map = new ol.Map({
    layers: [continentalLayer,alaskaLayer,hawaiiLayer,puertoRicoLayer],
    target: 'map',
    view: view,
    theme: null,
    moveTolerance: 10,
    controls: ol.control.defaults({
	zoom: true,
	rotate: false,
	attribution: false
    }).extend([
	// Custom controls
	// Resets view to original
	new ol.control.ZoomToExtent({
	    label: 'R',
	    tipLabel: 'Reset the view',
	})
    ]),
});


// Returns feature info for clicked on objects, on map
/* map.on('singleclick', function(evt) {
    document.getElementById('info').innerHTML = '';
    var viewResolution = @type {number}  (view.getResolution());
    var url = psql.getGetFeatureInfoUrl(
	evt.coordinate, viewResolution, 'EPSG:3857',
	{'INFO_FORMAT': 'text/html'});
    var url2 = continental.getGetFeatureInfoUrl(
	evt.coordinate, viewResolution, 'EPSG:3857',
	{'INFO_FORMAT': 'text/html'});
    if (url) {
	document.getElementById('info').innerHTML =
	    '<iframe seamless src="' + url + '"></iframe><iframe seamless src="' + url2 + '"></iframe>';};
    console.log(url);
});*/

// Loads map layers on drag
map.on('pointermove', function(evt) {
    if (evt.dragging) {
	return;
    }
    var pixel = map.getEventPixel(evt.originalEvent);
    var hit = map.forEachLayerAtPixel(pixel, function() {
	return true;
    });
    map.getTargetElement().style.cursor = hit ? 'pointer' : '';
});

// Stops canvas distortions if page is resized -- works sometimes
document.getElementsByTagName('canvas')[0].addEventListener('resize', function(event){
    map.updateSize();
});
window.addEventListener('resize', function(event){
    map.updateSize();
});

// Set viewport's ID here, instead of changing the ol.js code
document.getElementsByClassName('ol-viewport')[0].id='ol-viewport'


function __WFSRequestHandler__() {

    // Creates a WFS-compliant filter URL component from:
    // query operation ex. "greater than",
    // property to query against ex. "police stations",
    // and value to query with ex. "50"
    this.createFilter = function(operation, property, value) {
	var newParams = 
	    "<" + operation  + ">" +
	    "<PropertyName>" + property  + "</PropertyName>" +
	    "<Literal>" + value + "</Literal>" +
	    "</" + operation + ">";
	return newParams;
    }

    // Effectively "hashes" an object by converting its strings to an ASCII-8 bit array
    // of the underlying's hex (?) code
    this.hash = function(paramsObject) {
	return Int8Array.from(JSON.stringify(paramsObject));
    }

    // Checks to see if the new params' hash matches the internal one
    // Does this by comparing each key with the other's corresponding key
    // E.g oldHash[43] === newHash[43]
    // Exits if it meets any differiny values
    // Returns true if the hashes are different, false otherwise
    this.checkHash = function() {
	var hashDifferent = false;
	for (var i = 0, newHash = this.hash(this.createParamsList()),
		 length = newHash.length > this.paramsHash.length ?
		 newHash.length : this.paramsHash.length;
	     i < length; i++) {
	    if (newHash[i] === this.paramsHash[i]) {
		continue;
	    }
	    else {
		hashDifferent = true;
		break;
	    }
	}
	return hashDifferent;
    }

    
    // Default URI of the WFS server
    this.uri = 'http://127.0.0.1/wms/';

    // Default params to use if none are specified
    // If new params are specified, they must have defaults
    // Otherwise the url builder won't work/ stops short
    this.defaultParams = {
	'SERVICE': 'WFS',
	'VERSION': '2.0.0',
	'MAP': '/var/www/maps/wfs.map',
	'REQUEST': 'GetFeature',
	'TYPENAME': 'ms:psql',
	'PROPERTYNAME': 'fips',
	'SRS': 'EPSG:3857',
	'LEVEL':'county'
    };

    // Params list
    // Needed?
    this.params = {};

    // Params hash used in checking if params have changed
    this.paramsHash = this.hash(this.defaultParams);

    // Debug
    this.lastRequestOK = false;

    // URL used to call the WFS server, this one is the default
    this.url = 'http://127.0.0.1/wms/?' +
	'SERVICE=WFS' +
        '&VERSION=2.0.0' +
        '&MAP=/var/www/maps/wfs.map' +
        '&REQUEST=GetFeature' +
        '&TYPENAME=ms:psql' +
	'&LEVEL=' +
	document.getElementById("main-level-select").value +
        '&PROPERTYNAME=fips' +
        '&FILTER=' +
	this.defaultParams["FILTER"] +
        '&SRS=EPSG:3857';

    // Builds the URL needed to call the WFS server with the
    // appropriate options
    this.buildUrl = function(paramsList) {
	var newUrl = this.uri + '?';

	// For every item in the new params list
	// append it to the new URL
	// Else, append the default
	for (item in this.defaultParams) {
	    if (!paramsList[item]) {
		newUrl += '&' + item + '=' + this.defaultParams[item];
	    }
	    else {
		newUrl += '&' + item + '=' + paramsList[item];
	    }
	}
	if (paramsList['FILTER']) {
	    newUrl += '&FILTER=' + paramsList['FILTER'];
	};
	this.url = newUrl;
    };

    // Creates a portable parameter list using the selected options in the options page
    // This includes the filter and which properties are to be compared
    this.createParamsList = function() {
	var paramsList = {};
	var isNation = false;
	paramsList['PROPERTYNAME'] = 'fips,name';
	paramsList['LEVEL'] = document.getElementById("main-level-select").value;
	if (paramsList['LEVEL'] === 'nation') {
	    isNation = true;
	}
	var labelList = document.getElementsByClassName("extra-options-active")[0].getElementsByTagName("label");
	for (var i = 0, counter = 0; i < labelList.length; i++) {
	    if (labelList[i].children[0].value != "null") {
		counter++;
		if (counter === 1) {
		    paramsList['FILTER'] = '';
		}
		paramsList['PROPERTYNAME'] += ',' + labelList[i].children[0].dataset.sql;
		if (!isNation) {
		    paramsList['FILTER'] += this.createFilter(labelList[i].children[0].value,
							      labelList[i].children[1].dataset.sql,
							      labelList[i].children[1].value);
		}
	    }
	    if (i === labelList.length - 1 && !isNation) {
		if (counter > 1) {
		    paramsList['FILTER'] = "<AND>" + paramsList['FILTER'];
		}
		paramsList['FILTER'] = "<Filter>" + paramsList['FILTER'];
		if (counter > 1) {
		    paramsList['FILTER'] += "</AND>";
		}
		paramsList['FILTER'] += "</Filter>";
	    }
	}
	return paramsList;
    };
  
    // If the hash for both params are different, then generate a new URL
    // Otherwise return false
    this.getNewUrl = function() {
	if (this.checkHash()) {
	    var paramsList = this.createParamsList();
	    this.paramsHash = this.hash(paramsList);
	    this.buildUrl(paramsList);
	    return true;
	}
	else {
	    return false;
	};
    };
};


var wfsWrapper = new __WFSRequestHandler__;

// Global var pointing to "results" page
// For convenience 
var olResults = document.getElementById("results");

// Calls the WFS server, serializes the XML response
// Then updates the "results" page with the XML response
// in table form
function getFeatureInfoFromMap (element=null, action=null) {

    if (wfsWrapper.getNewUrl()) {
	
    // Init appearance
    olResults.classList.remove("results-loaded");
    olResults.classList.add("results-before");

    // Create CQL url
    olResults.textContent = 'Creating search request';
    
    // Client-facing load-status
    olResults.textContent = 'Starting request for results';
    
    // Create request object
    var xhr = new XMLHttpRequest();
    xhr.open("GET", wfsWrapper.url);
    olResults.textContent = 'Opening connection to server';
    xhr.responseType = "document";
    
    // Connect to WFS server
    xhr.send();

    // Serializes xhr.response if there is one
    // Then updates "results" with a custom table
	xhr.onload = function() {

	// Setup objects for parsing
	xhr.responseXML.documentElement.nodeName;
	var xmlText = new XMLSerializer().serializeToString(xhr.response);
	var parser = new DOMParser();
	xmlDoc = parser.parseFromString(xmlText, "text/xml");

	// Update "results"
	    try {

		// Generate the table from the results
		updateFeatureInfoFromXML(xmlDoc);

		// See if there are any results from the request
		// Change back the submit bar to normal if so
		if (element) {
		    element.setAttribute("onclick", action);
		    element.parentElement.style.backgroundColor = "";
		    element.classList.remove("disabled-button");
		    wfsWrapper.lastRequestOK = true;
		}
	    }

	    // Otherwise, fail gracefully if DOM errors pop up
	    catch (e){
		olResults.textContent = 'Getting results failed: ' + e;
		wfsWrapper.lastRequestOK = false;
	    }
	}
	
	xhr.onerror = function() {
	    dump("Error while getting XML.");
	    olResults.textContent = 'Failed to receive results from server.';
	}
    }
    else {
	return 1;
    }
    return 0;
};


// Global resultsList contains the data for the table
// resultsList[n] yields an object with several keys, depending on CQL specified in psqlUrl.
// Keys are named as the table headers. Values are the respective table row-cell values
// Ex. resultsList[n] = {"ms:fire_stations": "34", "ms:police_stations": "4", "ms:fips": "04230"}
var resultsList = []

// Build table from serialized XML
// Append built table to "results"
function updateFeatureInfoFromXML(xmlDoc) {

    // Check if there are any results, return otherwise
    if (xmlDoc.getElementsByTagName("ms:psql").length) { 
	
    // Clear results list for new XML results
    // If not cleared, will break sorting functions
    // Due to reliance on resultsList.length
    resultsList = [];

    // Setup unicode escape sequences for sort arrows
    var _unicode_upArrow = '\u{25B2}';
    var _unicode_downArrow = '\u{25BC}';
    var _unicode_sortArrows =
	' ' +
	_unicode_upArrow +
	' ' +
	_unicode_downArrow;
    
    // Create table and header
    var newTable = document.createElement("table");	    
    newTable.id = "results-table";
    var newTableHead = document.createElement("thead");
    newTable.appendChild(newTableHead);
    var newHeadRow = document.createElement("tr");
    newTableHead.appendChild(newHeadRow);

    //Initialize table header cells from XML results
    for (var featureChild = 0, featureList = xmlDoc.getElementsByTagName("ms:psql"); featureChild < featureList[0].childElementCount; featureChild++) {
	var newHeadCell = document.createElement("th");
	newHeadCell.textContent = featureList[0].children[featureChild].nodeName;
	newHeadRow.appendChild(newHeadCell);
    };

    // Create table body
    var newTableBody = document.createElement("tbody")
    newTable.appendChild(newTableBody);

    // Create body rows
    // Populate body row cells using serialized XML result data
    for (var featureNumber = 0, featureList = xmlDoc.getElementsByTagName("ms:psql");
	 featureNumber < featureList.length; featureNumber++) {

	// Create a new body row
	var newBodyRow = document.createElement("tr");

	// Set a unique value for the body row
	newBodyRow.dataset.primary_key = featureNumber;

	// Populate row cells
	for (var featureChild = 0; featureChild < featureList[featureNumber].childElementCount; featureChild++) {

	    // Create cell
	    var newBodyCell = document.createElement("td");

	    // Fill cell using serialized XML data
	    newBodyCell.textContent +=
		featureList[featureNumber].children[featureChild].textContent;
	    newBodyRow.appendChild(newBodyCell);

	    // Set the cell containin the "fips" data to "unique" for easier sorting
	    if (newHeadRow.children[featureChild].textContent === 'ms:fips') {
		newBodyCell.dataset.unique = true;
	    }
	};

	// Add JS function that highlights cell row onclick
	newBodyRow.onclick = function () {
	    if (this.style.backgroundColor == 'rgb(204, 204, 204)') {
		this.style.backgroundColor = '';
	    }
	    else {
		this.style.backgroundColor = 'rgb(204, 204, 204)';
	    };
	};
	
	// Append populated row to body
	newTableBody.appendChild(newBodyRow);

	// Create a new object mirroring the newly created row
	// Then add it to "resultsList" to mirror XML results
	holdList = {}
	for (var y = 0, found = false; y < newBodyRow.children.length; y++) {

	    // I don't remember what this does, looks useless
	    if (newBodyRow.children[y].dataset.unique && !found) {
		found = newBodyRow.children[y].textContent;
		y = -1;
	    }

	    // Copies cell data from body row to our mirror object
	    else if (found) {
		holdList[newHeadRow.children[y].textContent] =
		    newBodyRow.children[y].textContent;
	    }
	};
	
	// Copy all of the mirror object's data to the global resultList
	resultsList.push(holdList);
    };

    // Format design of results tab for table
    olResults.textContent = 'Results formatted.';
    olResults.classList.add("results-loaded");
    olResults.classList.remove("results-before");
    olResults.textContent = '';

    // Push the table to the results tab
    olResults.appendChild(newTable);

    // Add sort buttons to table header cells
    for (var i = 0; i < newHeadRow.children.length; i++) {

	var regex_ms = /\S+:/;
	var regex_underscore = /_/;
	
	// Copy the header cell's text for easier referencing
	var headerRowTextContent = newHeadRow.children[i].textContent;
	headerRowTextContent = newHeadRow.children[i].textContent.valueOf();
	newHeadRow.children[i].textContent = headerRowTextContent.replace(regex_ms, '');
	newHeadRow.children[i].textContent = headerRowTextContent.replace(regex_underscore, ' ');
	// Create and push up arrow, to header cell
	newUpArrow = document.createElement("span");
	newHeadRow.children[i].appendChild(newUpArrow);
	newUpArrow.textContent = ' ' + _unicode_upArrow + ' ';

	// Create and push down arrow, to header cell
	newDownArrow = document.createElement("span");
	newHeadRow.children[i].appendChild(newDownArrow);
	newDownArrow.textContent = ' ' +  _unicode_downArrow + ' ';

	// Add sort functionality for "onclick," to sort buttons
	var toNumber;
	switch (headerRowTextContent) {
	case 'ms:fips':
	case 'ms:name':
	    toNumber = false;
	    break;
	default:
	    toNumber = true;
	    break;
	}
	newUpArrow.setAttribute("onclick", "sortResultsTable(resultsList, '" + headerRowTextContent + "', " + toNumber + ", true)");
	newDownArrow.setAttribute("onclick", "sortResultsTable(resultsList, '" + headerRowTextContent + "', " + toNumber + ", false)");


	};
    }
    else {
	olResults.textContent = "No matches found.";
    };

    return;
};

// Pre-set buttons nodelist for reuse
buttons = document.getElementsByClassName('toggable-nav-button')

// Initialize default view instead of using default styles -- HACK?
for (i = 0; i < buttons.length; i++) {
    document.getElementById(buttons[i].dataset.view).classList.add('hidden');
}
// Less operations than using comparisons, starts page on "options" tab
document.getElementById('options').classList.remove('hidden');

// Turns on button's page, turns off everything else
// Matches the "data-view" of the button with the corresponding id in the "body-container"
function viewSwitcher (button) {
    for (i = 0; i < buttons.length; i++) {
	if (buttons[i] != button) {
	    buttons[i].classList.remove('active-button');
	    document.getElementById(buttons[i].dataset.view).classList.add('hidden');   
	} else {
	    button.classList.add('active-button');
	    document.getElementById(buttons[i].dataset.view).classList.remove('hidden');
	};
    };
};

// Event listener for every nav button
for (i = 0; i < buttons.length; i++) {
    buttons[i].addEventListener("click", function () {
	if (this.classList.contains('active-button')) {
	    return;
	} else {
	    viewSwitcher(this);
	};
    });
};

// When "All" is checked, check his siblings, else uncheck everything
function checkAll (allCheckBox) {
    nodeList = allCheckBox.parentElement.parentElement.getElementsByTagName('input');
    if (nodeList[0].checked === true) {
	for (nodePos = 1; nodePos < nodeList.length; nodePos++) {
	    nodeList[nodePos].checked=true;
	};
	document.getElementById('areas-selected').innerHTML = 51;
    } else {
	for (nodePos = 1; nodePos < nodeList.length; nodePos++) {
	    nodeList[nodePos].checked=false;
	};
	document.getElementById('areas-selected').innerHTML = 0;
    };
};

// All checkbox event listener
document.getElementById('supp-options').getElementsByTagName('input')[0].addEventListener("click", function () {
    checkAll(this);
});

// Most atomic way to recheck "Areas" number, expensive
// Check how many checkboxes are checked, and change the "Areas" number
// to the amount
function updateAreaCountGlobal () {
    if (document.getElementById('main-options').getElementsByTagName('select')[0].value == 'nation') {
	document.getElementById('areas-selected').innerText = 'X';
    }
    else if (document.getElementById('supp-options').getElementsByTagName('input')[0].checked == 'true') {
	document.getElementById('areas-selected').innerText = '51';
    }
    else {
	count = 0;
	for (i = 1, checkBoxes = document.getElementById('supp-options').getElementsByTagName('input'); i < checkBoxes.length; i++) {
	    if (checkBoxes[i].checked == true) {
		count++;
	    };
	};
	document.getElementById('areas-selected').innerText = count;
    };
};

// Init areas checked, in case checkboxes were saved
updateAreaCountGlobal();

// Add area recheck to all checkboxes after 'All'
for (i = 1; i < document.getElementById('supp-options').getElementsByTagName('input').length; i++) {
    document.getElementById('supp-options').getElementsByTagName('input')[i].addEventListener("change", function () {
	updateAreaCountGlobal();
    });
};

// State tracker for psqlLayer
var extraOptionsCounter = 0;

// Main handler for changing extra-options options boxes
function resizeOptionDropdown (optionBox) {
    optionBoxInput = optionBox.parentElement.getElementsByTagName('input')[0]
    if (optionBox.value !== 'null') {
	extraOptionsCounter++;
	optionBoxInput.value="";
	switch (optionBoxInput.dataset.type) {
	case 'number':
	    optionBoxInput.style.display='inline-block';
	    optionBox.style.width = '150px';
	    optionBoxInput.step = 1;
	    optionBoxInput.min = 0;
	    optionBoxInput.value = 1;
	    optionBoxInput.max = '';
	    optionBoxInput.style.width = '75px';
	    break;
	case 'nation':
	    optionBox.style.width = '80px';
	    break;
	default:
	    console.log(''.concat('Wrong optionBox value: ', optionBoxInput.type))
	    break;
	}
	    
    } else {
	extraOptionsCounter--
	optionBox.style.width='25px';
	optionBoxInput.value=''
	optionBoxInput.style.display='';
    };
}

// Changes what 'extra-info' is visible, depending on the Specificty selected
function setExtraOptionsActive (optionRefers) {
    var extraOptionsViews = document.getElementsByClassName('extra-options');
    for (i = 0; i < extraOptionsViews.length; i++) {
	if (extraOptionsViews[i].id != optionRefers) {
	    extraOptionsViews[i].classList.remove('extra-options-active');
	    extraOptionsViews[i].classList.add('hidden');   
	} else {
	    extraOptionsViews[i].classList.add('extra-options-active');
	    extraOptionsViews[i].classList.remove('hidden');
	};
    };

    // If "Nation" is chosen, disable area selection
    if (optionRefers === 'extra-options-nation') {
	for (i = 0, checkBoxes = document.getElementById('supp-options').getElementsByTagName('input'); i < checkBoxes.length; i++){
	    checkBoxes[i].disabled = 'disabled';
	};
	updateAreaCountGlobal();
    }

    // Otherwise, set all checkboxes to allowed
    else {
	for (i = 0, checkBoxes = document.getElementById('supp-options').getElementsByTagName('input'); i < checkBoxes.length; i++){
	    checkBoxes[i].disabled = '';
	};
	updateAreaCountGlobal();
    };
};

// Event listener for specificity type
document.getElementById('main-options').getElementsByTagName('select')[0].addEventListener("change", function () {
    setExtraOptionsActive("".concat('extra-options-',this.value));
    
});

// Init the Extra Options
setExtraOptionsActive(''.concat('extra-options-',document.getElementById('main-options').getElementsByTagName('select')[0].value));

// Init the box appearances
for (z = 0; z < document.getElementsByClassName('extra-options').length; z++) {
    for (i = 0, optionBoxes = document.getElementsByClassName('extra-options')[z].getElementsByTagName('select'); i < optionBoxes.length; i++) {
	resizeOptionDropdown(optionBoxes[i]);
    };
};

// Reset the counter after initializing boxes
extraOptionsCounter = 0;

// Init layers list -- easier than messing around with map.getLayers()
var mapLayerStates = {"continentalLayer":continentalLayer,"hawaiiLayer":hawaiiLayer,"alaskaLayer":alaskaLayer,"puertoRicoLayer":puertoRicoLayer/*,"psqlLayer":psqlLayer*/};

// Listener for function that sets map layers depending on "options" settings
// Set on the "Map" nav-tab button
//document.getElementById('map-button').addEventListener('click', updateMapLayersFromOptions());

// Turns map layers visible, corresponding to the "Options" tab options
function updateMapLayersFromOptions () {
    
    // If the entire "Nation" is chosen, turn everything on
    if (document.getElementById('main-options').getElementsByTagName('select')[0].value == 'nation') {
	for (layerState in mapLayerStates) {
	    if (mapLayerStates[layerState].getVisible() === false) {
		mapLayerStates[layerState].setVisible(true);
	    };
	};
    }
    else {
	// Init continental layer to invisible, instead of checking each state's state
	continentalLayer.setVisible(false);
	
	// Init layer state switches
	continentalSwitch = false;
	alaskaSwitch = false;
	hawaiiSwitch = false;
	puertoRicoSwitch = false;
	
	// Iterate over all of the checkboxes under "Areas" and turn on their
	// respective areas.
	for (i = 1, checkBoxes = document.getElementById('supp-options').getElementsByTagName('input'); i < checkBoxes.length; i++) {

	    // Get the names of the territories, next to the checkboxes
	    territory = document.getElementById("supp-options").getElementsByTagName("form")[0].getElementsByTagName("label")[i].innerText;
    
	    // If checkbox is checked, turn on respective layers
	    if (checkBoxes[i].checked == true) {
		switch (territory) {
		case 'Alaska':
		    if (alaskaLayer.getVisible() === false) {
			alaskaLayer.setVisible(true);
		    };
		    alaskaSwitch = true;
		    break;
		case 'Hawaii':
		    if (hawaiiLayer.getVisible() === false) {
			hawaiiLayer.setVisible(true);
		    };
		    hawaiiSwitch = true;
		    break;
		case 'Puerto Rico':
		    if (puertoRicoLayer.getVisible() === false) {
			puertoRicoLayer.setVisible(true);
		    };
		    puertoRicoSwitch = true;
		    break;
		default:
		    // Catch-all for continental states
		    continentalSwitch = true;
		    continentalLayer.setVisible(true);
		};
	    }
	    
	    // If checkbox isn't checked, turn off respective layers
	    else {
		switch (territory) {
		case 'Alaska':
		    if (alaskaLayer.getVisible() === true) {
			alaskaLayer.setVisible(false);
		    };
		    break;
		case 'Hawaii':
		    if (hawaiiLayer.getVisible() === true) {
			hawaiiLayer.setVisible(false);
		    };
		    break;
		case 'Puerto Rico':
		    if (puertoRicoLayer.getVisible() === true) {
			puertoRicoLayer.setVisible(false);
		    };
		    break;
		default:
		    break;
		};
	    };
	};
	// Set zoom and center of map for specific cases
	switch(true){
	case continentalSwitch:
	    switch (true) {
	    case alaskaSwitch && hawaiiSwitch && puertoRicoSwitch:
	    case alaskaSwitch && hawaiiSwitch:
	    case alaskaSwitch && puertoRicoSwitch:
		map.getView().setCenter(locations["all"][0]);
		map.getView().setZoom(locations["all"][1]);
		break;
	    case alaskaSwitch:
		map.getView().setCenter(locations["continental&alaska"][0]);
		map.getView().setZoom(locations["continental&alaska"][1]);
		break;
	    case hawaiiSwitch && puertoRicoSwitch:
	    case hawaiiSwitch:
		map.getView().setCenter(locations["continental&hawaii"][0]);
		map.getView().setZoom(locations["continental&hawaii"][1]);
		break;
	    case puertoRicoSwitch:
		map.getView().setCenter(locations["continental&puertoRico"][0]);
		map.getView().setZoom(locations["continental&puertoRico"][1]);
		break;
	    default:
		map.getView().setCenter(locations["continental"][0]);
		map.getView().setZoom(locations["continental"][1]);
	    }
	    break;
	case alaskaSwitch:
	    switch (true) {
	    case hawaiiSwitch && puertoRicoSwitch:
		map.getView().setCenter(locations["all"][0]);
		map.getView().setZoom(locations["all"][1]);
		break;
	    case hawaiiSwitch:
		map.getView().setCenter(locations["alaska&hawaii"][0]);
		map.getView().setZoom(locations["alaska&hawaii"][1]);
		break;
	    case puertoRicoSwitch:
		map.getView().setCenter(locations["alaska&puertoRico"][0]);
		map.getView().setZoom(locations["alaska&puertoRico"][1]);
		break;
	    default:
		map.getView().setCenter(locations["alaska"][0]);
		map.getView().setZoom(locations["alaska"][1]);
	    }
	    break;
	case hawaiiSwitch:
	    switch (true) {
	    case puertoRicoSwitch:
		map.getView().setCenter(locations["hawaii&puertoRico"][0]);
		map.getView().setZoom(locations["hawaii&puertoRico"][1]);
		break;
	    default:
		map.getView().setCenter(locations["hawaii"][0]);
		map.getView().setZoom(locations["hawaii"][1]);
	    }
	    break;
	case puertoRicoSwitch:
	    map.getView().setCenter(locations["puertoRico"][0]);
	    map.getView().setZoom(locations["puertoRico"][1]);
	    break;
	};
	/*
	if (extraOptionsCounter === 0) {
	        psqlLayer.setVisible(false);
	}
	else {
	    psqlLayer.setVisible(true);
	}
	*/
    };
    
    // Set reset view button's "extent" / coordinates to reset to
    map.getControls().getArray()[1].extent = map.getView().calculateExtent();
};

// Sort table cell results
// arr is resultsList of the mirrored data
// toSort is the name of the property to sort
// Ex. "ms:fips"
// toNum determines if the property should be converted to a numeric first
// because text doesn't get sorted by numerical value
// Reverse determines if the the sorted objects should be descending order or not
// E.g true means results will go like: 500 400 300 200 100
// E.g false means results will go like: 100 200 300 400 500
function sortResultsTable(arr, toSort, toNum, reverse) {
    // Convert the property values to numbers if needed, breaks fips numbers
    if (toNum === true) {
	for (var i = 0, length = arr.length; i < length; i++) {
	    arr[i][toSort] = parseInt(arr[i][toSort], 10);
	}
    }
    
    // Start merging and updating the table
    updateResultsTable(mergeSort(arr, toSort), reverse);

};

// Update the result table's cell to reflect a sorted state
function updateResultsTable(arr, toReverse) {

    // Update the cells in descending order
    if (toReverse) {
	for (var i = 0; i < arr.length; i++) {
	    var n = 0;
	    for (item in arr[0]) {
		document.getElementsByTagName("tbody")[0].children[(arr.length - i - 1)].children[n].textContent =
		    arr[i][item];
		n++;
	    };
	};
    }

    // Update the cells in ascending order
    else {
	for (var i = 0; i < arr.length; i++) {
	    var n = 0;
	    for (item in arr[0]) {
		document.getElementsByTagName("tbody")[0].children[i].children[n].textContent =
		    arr[i][item];
		n++;
	    };
	};
    };
};

// Sort the array's values by top-down mergesort
function mergeSort(arr, toSort) {

    // Base case
    if (arr.length <= 1) {
        return arr;
    };

    // Recursive, divide and conquer, case
    var middle = parseInt(arr.length / 2);
    var left = arr.slice(0, middle);
    var right = arr.slice(middle, arr.length);
    return merge(mergeSort(left, toSort), mergeSort(right, toSort), toSort);
};

function merge(left, right, toSort) {
    var result = [];

    while (left.length && right.length) {
        if (left[0][toSort] <= right[0][toSort]) {
            result.push(left.shift());
        } else {
            result.push(right.shift());
        };
    };

    while (left.length)
        result.push(left.shift());

    while (right.length)
        result.push(right.shift());

    return result;
};

// Controls what happens when submit is hit on "options"
function submitOptions(submitButton) {

    // Save the current onclick function to add back after temporary removal
    var onClick = submitButton.getAttribute("onclick")

    // Remove current onclick function so its effectively bricked
    // until results are received --
    // slows down requests
    submitButton.setAttribute("onclick", "");
    // Grey out background and disable hover animations
    submitButton.parentElement.style.backgroundColor = '#c8c8c8';
    submitButton.classList.add("disabled-button");

    // Check if a "Go To" option is selected
    // switch to corresponding view if so
    if (document.getElementById("goto-select").children[0].value !== "null") {
	for (i = 0, buttons = document.getElementsByTagName("nav")[0].children; i < buttons.length; i++) {
	    if (document.getElementById("goto-select").children[0].value === buttons[i].dataset.view) {
		viewSwitcher(buttons[i]);
	    };
	};
    };

    // Change the layers to the ones selected in the options
    updateMapLayersFromOptions();

    // Make a WFS request to the server
    // Else, reset the submit bar's styles
    if (!getFeatureInfoFromMap(submitButton, onClick)) {
	olResults.textContent = 'Results received: Building table';
    }
    else {
	submitButton.setAttribute("onclick", onClick);
	submitButton.parentElement.style.backgroundColor = "";
	submitButton.classList.remove("disabled-button");
    }

};
