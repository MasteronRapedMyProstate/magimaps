#!/usr/local/bin/bash
set -ex;

readonly MAGIMAPS_DEBUG=${MAGIMAPS_DEBUG:='0'};

# Setup basic user-control
query_response()
{
    if [[ "$MAGIMAPS_DEBUG" != 0 ]]; then
	read -p "Continue? [Y/n]: " ANSWER;
	if [[ "$ANSWER" == [Yy] ]]; then
	    ANSWER='';
	    :; # Noop/Continue
	else
	    exit 1;
	fi
    fi
}

# Project root
MAGIMAPS='/home/alexander/Documents/Projects/magiMaps';

# Move into srcs directory
cd "$MAGIMAPS"; mkdir -p 'src'; cd 'src';

# Pull current mapcache sources
if [ -d "mapcache" ]; then
    cd mapcache; rm -rf build; git reset --hard; git pull;
else
    git clone 'git://github.com/mapserver/mapcache.git';
    cd mapcache;
fi

# Initialize files in mapcache sources
cp 'nginx/config.in' 'nginx/config';

mkdir 'build'; cd 'build';
cmake -DCMAKE_INSTALL_PREFIX="$MAGIMAPS"'/usr/local'                  \
		 -DWITH_GEOS=0                                        \
		 -DWITH_GDAL=0                                        \
		 -DWITH_OGR=0 ..;

# Let user check cmake output
query_response

# Build and install mapcache
make; su -L root -c 'make install; ln -fs '"$MAGIMAPS"'/usr/local/lib/'"$MAGIMAPS"'/usr/lib'; 

# Nginx download link
NGINX_URL='http://nginx.org/download/nginx-1.13.12.tar.gz';

# Download nginx source tarball
cd "$MAGIMAPS"'/src';

rm -rf nginx/; mkdir nginx; cd nginx; wget "$NGINX_URL"; wget "$NGINX_URL"'.asc';

# Shorthand for package
NGINX_TAR=${NGINX_URL:26:100};

# Get Nginx PGP key and verify sigs
gpg2 --keyserver hkp://ipv4.pool.sks-keyservers.net:80 --recv-keys 520A9993A1C052F8;
gpg2 --verify "$NGINX_TAR"'.asc' "$NGINX_TAR";

# Let user read gpg output before continuing
query_response;

# Extract Nginx source and clean up
tar -xzvf "$NGINX_TAR"; rm "$NGINX_TAR";

# Move into tarred directory
cd $(sed 's/.\{7\}$//g' <(echo "$NGINX_TAR"));

./configure                                                   \
    --add-module="$MAGIMAPS"'/src/mapcache/build/nginx'       \
    --prefix="$MAGIMAPS"'/usr/local/nginx'                    \
    --sbin-path="$MAGIMAPS"'/usr/local/sbin/nginx'            \
    --conf-path="$MAGIMAPS"'/etc/nginx/nginx.conf'            \
    --modules-path="$MAGIMAPS"'/etc/nginx/modules'            \
    --error-log-path='/var/nginx/log/error.log'               \
    --pid-path='/var/run/nginx.pid'                           \
    --lock-path='/var/run/nginx.lock'                         \
    --user=www                                                \
    --group=www                                               \
    --http-log-path='/var/nginx/log/access.log'               \
    --http-client-body-temp-path='/var/nginx/client_body_temp'\
    --http-proxy-temp-path='/var/nginx/proxy_temp'            \
    --http-fastcgi-temp-path='/var/nginx/fastcgi_temp'        \
    --http-uwsgi-temp-path='/var/nginx/uwsgi_temp'            \
    --http-scgi-temp-path='/var/nginx/scgi_temp';

# Let user read ./configure output before continuing
query_response;

make; su -L root -c 'make install';
